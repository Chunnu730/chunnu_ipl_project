const fs = require('fs');
const csv = require('csvtojson');
const deliveriesCsvFilePath = "./src/data/deliveries.csv";
csv()
.fromFile(deliveriesCsvFilePath)
.then((jsonObj) => {
  fs.writeFileSync('./src/data/deliveries.js',JSON.stringify(jsonObj,null,4), "utf-8" , (error) => {
    if(error){
      console.log(error);
    }
  });
});