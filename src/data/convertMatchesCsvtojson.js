const fs = require('fs');
const csv = require('csvtojson');
const matchesCsvFilePath = "./src/data/matches.csv";
csv()
.fromFile(matchesCsvFilePath)
.then((jsonObj) => {
  fs.writeFileSync('./src/data/matches.js',JSON.stringify(jsonObj,null,4), "utf-8" , (error) => {
    if(error){
      console.log(error);
    }
  });
});