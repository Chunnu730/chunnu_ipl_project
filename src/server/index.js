const saveData = require('./saveData.js');
const matchesData = require('../data/matches.js');
const deliveriesData = require('../data/deliveries.js');
const matchesPlayedPerYear = require('./matchesPlayedPerYear.js');
const matchesWonByTeamPerYear = require('./matchesWonByTeamPerYear.js');
const extraRunConcededPerTeamIn2016 = require('./extraRunConcededPerTeamIn2016.js');
const topTenEconomicalBowlersIn2015 = require('./topTenEconomicalBowlersIn2015.js');


const result1 = matchesPlayedPerYear(matchesData);
const pathWhereToSave1 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src/public/output/matchesPlayesPerYear.json';
saveData(result1, pathWhereToSave1);


const result2 = matchesWonByTeamPerYear(matchesData);
const pathWhereToSave2 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src/public/output/matchesWonByTeamPerYear.json';
saveData(result2, pathWhereToSave2);

const result3 = extraRunConcededPerTeamIn2016(matchesData, deliveriesData);
const pathWhereToSave3 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src/public/output/extraRunConcededPerTeanIn2016.json';
saveData(result3, pathWhereToSave3);


const result4 = topTenEconomicalBowlersIn2015(matchesData, deliveriesData);
const pathWhereToSave4 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src/public/output/top10EconomicalBowler.json';
saveData(result4, pathWhereToSave4);