function matchesPlayedPerYear(matchesData){

    let getMatchesPlayedPerYear = matchesData.reduce((accumulator , currentElement) => {
        if(accumulator.hasOwnProperty(currentElement.season)){

            accumulator[currentElement.season] = accumulator[currentElement.season] + 1;

        }else{

            accumulator[currentElement.season] = 1;
        }

        return accumulator;
    },{});
    
    return getMatchesPlayedPerYear;
}
module.exports = matchesPlayedPerYear;
