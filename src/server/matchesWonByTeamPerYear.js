function matchesWonByTeamPerYear(matchesData) {

  let getMatchesWonByTeamPerYear = matchesData.reduce((accumulator , currentElement) => {

      if(accumulator.hasOwnProperty(currentElement.team1) && accumulator[currentElement.team1]['year'] === currentElement.season){

        if(currentElement.team1 === currentElement.winner){

          accumulator[currentElement.team1]['wons'] = accumulator[currentElement.team1]['wons']+1;

        }else{

          accumulator[currentElement.team1]['wons'] = accumulator[currentElement.team1 ]['wons']+0;
        }

      }else{
          accumulator[currentElement.team1] = {'year' : currentElement.season , 'wons': 1};
      }
      return accumulator;
  },{});
  
  return getMatchesWonByTeamPerYear;
}

module.exports = matchesWonByTeamPerYear;
