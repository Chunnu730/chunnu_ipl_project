function extraRunConcededPerTeamIn2016(matchesData, deliveriesData) {
    let matchIdOf2016 = matchesData.filter((elements) => elements.season === '2016').map((elements) => {
        return elements.id;
    });

    let getExtraRunConcededPerTeamIn2016 = deliveriesData.reduce((accumulator, currentElement) => {
        if (matchIdOf2016.includes(currentElement.match_id)) {

            if (accumulator.hasOwnProperty(currentElement.bowling_team)) {
                
                accumulator[currentElement.bowling_team] = accumulator[currentElement.bowling_team] + parseInt(currentElement.extra_runs);

            } else {

                accumulator[currentElement.bowling_team] = parseInt(currentElement.extra_runs);

            }
        }
        return accumulator;

    }, {});

    return getExtraRunConcededPerTeamIn2016;
}

module.exports = extraRunConcededPerTeamIn2016;