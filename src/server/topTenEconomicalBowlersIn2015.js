function topTenEconomicalBowlersIn2015(matchesData, deliveriesData) {

    let matchIdOf2015 = matchesData.filter((elements) => elements.season === '2015').map((elements) => {
        return elements.id;
    });

    let getBowlersRunsBallsIn2015 = deliveriesData.reduce((accumulator, currentElement) => {

        if (matchIdOf2015.includes(currentElement.match_id)) {

            if (accumulator.hasOwnProperty(currentElement.bowler)) {
                accumulator[currentElement.bowler]['runs'] = accumulator[currentElement.bowler]['runs'] + parseInt(currentElement.total_runs);
                accumulator[currentElement.bowler]['balls'] = accumulator[currentElement.bowler]['balls'] + 1;

            } else {
                accumulator[currentElement.bowler] = { "runs": parseInt(currentElement.total_runs), "balls": 1 };
            }
        }
        return accumulator;
    }, {});

    let getEconomicalBowlersIn2015 = Object.keys(getBowlersRunsBallsIn2015).reduce((accumulator, bowler) => {

        accumulator[bowler] = ((getBowlersRunsBallsIn2015[bowler]['runs'] * 6) / (getBowlersRunsBallsIn2015[bowler]['balls']));
        return accumulator;

    }, {});

    let gettopTenEconomicalBowlersIn2015 = Object.entries(getEconomicalBowlersIn2015).sort((firstElement, secondElement) => {

        return firstElement[1] - secondElement[1];

    }).filter((element, index) => index<10);

    return Object.fromEntries(gettopTenEconomicalBowlersIn2015);
}

module.exports = topTenEconomicalBowlersIn2015;