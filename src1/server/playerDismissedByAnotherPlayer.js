function playerDismissedByAnotherPlayer(deliveriesData){

    let dismissedPlayer = deliveriesData.reduce((accumulation , current) => {

        if (current.batsman === current.player_dismissed) {

            if (!accumulation.hasOwnProperty(current.batsman)) {
                accumulation[current.batsman]= {};
                accumulation[current.batsman][current.bowler] = 1;
            }
            else {

                if (!accumulation[current.batsman].hasOwnProperty(current.bowler)) {
                   accumulation[current.batsman][current.bowler] =1;
                }

                else {
                    accumulation[current.batsman][current.bowler] = accumulation[current.batsman][current.bowler]+1;
                }
            }
        }
        return accumulation;

    }, {});
    let result=Object.fromEntries(Object.entries(dismissedPlayer).map((element) => {
       return [element[0], Object.values(element[1]).sort((first ,second) =>{
           return first-second;
       }).reverse().filter((current ,index) => index <1)];
    }));
   return result;
    
}

module.exports = playerDismissedByAnotherPlayer;