function teamWonTossAndMatch(matchesData){
    let getTeamWonTossAndMatch = matchesData.reduce((accumulation , currentElement) => {
        if(currentElement.team1 === currentElement.toss_winner && currentElement.team1 === currentElement.winner){
          if(accumulation.hasOwnProperty(currentElement.team1)){
             accumulation[currentElement.team1] = accumulation[currentElement.team1]+1;
          }else{
              accumulation[currentElement.team1] = 1;
          }
        }
        return accumulation;
    },{});
    return getTeamWonTossAndMatch;
}

module.exports = teamWonTossAndMatch;