function bowlerWithBestEconomyInSuperOver(deliveriesData) {

    let getEconomicalBowler = deliveriesData.reduce((accumulation, current) => {

        if (current.is_super_over == 1) {
            let overAllRuns = parseInt(current.wide_runs) + parseInt(current.noball_runs) + parseInt(current.batsman_runs);

            if (accumulation.hasOwnProperty(current.bowler)) {

                accumulation[current.bowler]['runs'] += overAllRuns;
                accumulation[current.bowler]['balls']++;
            }
            else {
                accumulation[current.bowler] = {};
                accumulation[current.bowler]['runs'] = overAllRuns;
                accumulation[current.bowler]['balls'] = 1;


            }
        }

        return accumulation;

    }, {});

    let bowlerWithBestEconomyInSuperOver = Object.fromEntries(Object.entries(getEconomicalBowler).map((element) => {
        return [element[0], (parseInt(element[1].runs) * 6) / parseInt(element[1].balls)];
    }).sort((first, second) => {
        return first[1] - second[1];
    }).filter((current, index) => index < 1));
    return bowlerWithBestEconomyInSuperOver;
}

module.exports = bowlerWithBestEconomyInSuperOver;