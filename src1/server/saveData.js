const fs = require('fs');
function saveData(result,pathWhereToSave) {
    const JsonData = JSON.stringify(result, null, 2);
    fs.writeFile(pathWhereToSave, JsonData, (error) => {
        if (error) {
            console.log(error);
        }
    });
}
module.exports = saveData;