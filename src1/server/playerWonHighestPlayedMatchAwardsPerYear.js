function playerWonHighestPlayedMatchAwardsPeryear(matchesData){
    let playersOfMatchPerYear = matchesData.reduce((accumulation,currentElement) => {
        if(accumulation.hasOwnProperty(currentElement.season)){
            if(accumulation[currentElement.season].hasOwnProperty(currentElement.player_of_match)){
                accumulation[currentElement.season][currentElement.player_of_match]++;
            }else{
                accumulation[currentElement.season][currentElement.player_of_match] =1;
            }
        }else{
            accumulation[currentElement.season] ={};
            accumulation[currentElement.season][currentElement.player_of_match] = 1;

        }
        return accumulation;
    },{});
   
    let highestPlayerWonMatchAwards = Object.entries(playersOfMatchPerYear).map((element) => {
        return [element[0], Object.fromEntries(Object.entries(element[1]).sort((first, second) => {
            return first[1] - second[1];
         }).reverse().filter((element, index) => index < 1))];
     }
     );
    return  Object.fromEntries(highestPlayerWonMatchAwards);
}

module.exports = playerWonHighestPlayedMatchAwardsPeryear;
