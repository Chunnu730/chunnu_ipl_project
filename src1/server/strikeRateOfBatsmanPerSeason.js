function strikeRateOfBatsmanPerSeason(matchesData, deliveriesData) {

    let yearByMatchId = matchesData.reduce((accumulation,current)=>{
         accumulation[current.id] = {};
         accumulation[current.id] = current.season;
         return accumulation;
    },{});

    let getBatsmanRunsAndBalls = deliveriesData.reduce((accumulation, current) => {
             
        if (accumulation.hasOwnProperty(yearByMatchId[current.match_id])) {
             
            if (accumulation[yearByMatchId[current.match_id]].hasOwnProperty(current.batsman)) {

                accumulation[yearByMatchId[current.match_id]][current.batsman]['runs'] = accumulation[yearByMatchId[current.match_id]][current.batsman]['runs'] + parseInt(current.batsman_runs);

                accumulation[yearByMatchId[current.match_id]][current.batsman]['balls']++;

            } else {

                accumulation[yearByMatchId[current.match_id]][current.batsman] = {};

                accumulation[yearByMatchId[current.match_id]][current.batsman]['runs'] = parseInt(current.batsman_runs);

                accumulation[yearByMatchId[current.match_id]][current.batsman]['balls'] = 1;

            }
        } else {

            accumulation[yearByMatchId[current.match_id]] = {}

            accumulation[yearByMatchId[current.match_id]][current.batsman] = {};

            accumulation[yearByMatchId[current.match_id]][current.batsman]['runs'] = parseInt(current.batsman_runs);

            accumulation[yearByMatchId[current.match_id]][current.batsman]['balls'] = 1;
        }
        return accumulation;

    }, {});
    

   let getStrikeRateOfBatsmanPerSeason = Object.keys(getBatsmanRunsAndBalls).reduce((accumulation,current1) => {

       accumulation[current1] ={};

       accumulation[current1] = Object.keys(getBatsmanRunsAndBalls[current1]).reduce((accumulation,current2)=>{

          accumulation[current2] = {};
          accumulation[current2] = (getBatsmanRunsAndBalls[current1][current2]['runs']/getBatsmanRunsAndBalls[current1][current2]['balls'])*100;
          return accumulation;

       },{});

       return accumulation;

   },{});
   
   return getStrikeRateOfBatsmanPerSeason;
}

module.exports = strikeRateOfBatsmanPerSeason;