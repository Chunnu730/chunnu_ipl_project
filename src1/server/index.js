const saveData = require('./saveData.js');
const deliveriesData = require('../data/deliveries.js');
const matchesData = require('../data/matches.js');
const teamWonTossAndMatch = require('./teamWonTossAndMatch.js');
const playerWonHighestPlayedMatchAwardsPeryear = require('./playerWonHighestPlayedMatchAwardsPerYear.js');
const strikeRateOfBatsmanPerSeason = require('./strikeRateOfBatsmanPerSeason.js');
const playerDismissedByAnotherPlayer = require('./playerDismissedByAnotherPlayer.js');
const bowlerWithBestEconomyInSuperOver = require('./bowlerWithBestEconomyInSuperOver.js');


const result1 = teamWonTossAndMatch(matchesData);
const pathWhereToSave1 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src1/public/output/teamWonTossAndMatch.json';
saveData(result1, pathWhereToSave1);


const result2 = playerWonHighestPlayedMatchAwardsPeryear(matchesData);
const pathWhereToSave2 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src1/public/output/playerWonHighestPlayedMatchAwardsPerYear.json';
saveData(result2, pathWhereToSave2);


const result3 = strikeRateOfBatsmanPerSeason(matchesData,deliveriesData);
const pathWhereToSave3 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src1/public/output/strikeRateOfBatsmanPerSeason.json';
saveData(result3, pathWhereToSave3);


const result4 = playerDismissedByAnotherPlayer(deliveriesData);
const pathWhereToSave4 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src1/public/output/playerDismissedByAnotherPlayer.json';
saveData(result4, pathWhereToSave4);


const result5 = bowlerWithBestEconomyInSuperOver(deliveriesData);
const pathWhereToSave5 = '/home/chunnu/Desktop/Chunnu_IPL_Project/src1/public/output/bowlerWithBestEconomyInSuperOver.json';
saveData(result5, pathWhereToSave5);



